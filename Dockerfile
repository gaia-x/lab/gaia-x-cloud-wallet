FROM node:20-buster as development-build-stage

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install --only=development

COPY . .

RUN npm run build

# Production Stage
FROM node:20-buster as production-build-stage

ENV NODE_ENV=production

WORKDIR /usr/src/app

COPY --from=development-build-stage /usr/src/app/node_modules ./node_modules
COPY --from=development-build-stage /usr/src/app/dist ./dist

COPY package*.json ./

COPY . .

CMD ["node", "dist/src/main"]
