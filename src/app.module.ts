import { Module } from '@nestjs/common'
import { AppController } from './app.controller'
import { OIDC4VCIService } from './oidc4vci/oidc4vci.service'
import publicKeyProvider from './provider/public-key.provider'
import privateKeyProvider from './provider/private-key.provider'
import { ExpirableStorageService } from './storage/storage.service'
import credentialsSupport from './provider/credentials-support.provider'
import { ConfigModule } from '@nestjs/config'
import { Oidc4VCIController } from './oidc4vci/oidc4vci.controller'
import { InMemoryStorageService } from '@gaia-x/oidc4vc'
import { S3StorageService } from './storage/s3-storage.service'
import { StorageExpirationService } from './storage/storage-expiration.service'
import { ScheduleModule } from '@nestjs/schedule'

@Module({
  imports: [ConfigModule.forRoot(), ScheduleModule.forRoot()],
  controllers: [AppController, Oidc4VCIController],
  providers: [
    StorageExpirationService,
    OIDC4VCIService,
    publicKeyProvider,
    privateKeyProvider,
    { provide: ExpirableStorageService, useFactory: () => (process.env.STORAGE === 'S3' ? new S3StorageService() : new InMemoryStorageService()) },
    { provide: 'credentialsSupport', useValue: credentialsSupport }
  ]
})
export class AppModule {}
