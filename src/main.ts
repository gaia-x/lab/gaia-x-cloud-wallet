import { HttpsOptions } from '@nestjs/common/interfaces/external/https-options.interface'
import { NestFactory } from '@nestjs/core'
import { SwaggerModule } from '@nestjs/swagger'
import { DocumentBuilder } from '@nestjs/swagger/dist'
import { AppModule } from './app.module'
import { description, name, version } from '../package.json'

const appPath = !!process.env['APP_PATH'] ? process.env['APP_PATH'] : ''

async function bootstrap() {
  const httpsOptions: HttpsOptions = process.env.CERTIFICATE && {
    key: process.env.PRIVATE_KEY,
    cert: process.env.CERTIFICATE
  }
  const app = await NestFactory.create(AppModule, { httpsOptions })
  app.enableCors()
  app.setGlobalPrefix(`${appPath}/`)
  const config = new DocumentBuilder().setTitle(name).setDescription(description).setVersion(version).build()
  const document = SwaggerModule.createDocument(app, config)
  SwaggerModule.setup(`${appPath}/docs`, app, document)

  await app.listen(parseInt(process.env.PORT || '3000'))
}
bootstrap()
