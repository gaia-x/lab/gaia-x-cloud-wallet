import { Controller, Get } from '@nestjs/common'
import { ApiOperation } from '@nestjs/swagger'
import { description, name, version } from '../package.json'

@Controller()
export class AppController {
  @ApiOperation({ summary: 'Get app description' })
  @Get()
  getDescription() {
    return {
      software: name,
      description,
      version,
      documentation: `${process.env.BASE_URL}/docs/`
    }
  }
}
