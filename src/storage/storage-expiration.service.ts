import { Injectable } from '@nestjs/common'
import { Inject } from '@nestjs/common/decorators'
import { Logger } from '@nestjs/common/services'
import { Cron } from '@nestjs/schedule'
import { CronExpression } from '@nestjs/schedule/dist'
import { ExpirableStorageService } from './storage.service'

@Injectable()
export class StorageExpirationService {
  private readonly expireAfterMs: number

  constructor(@Inject(ExpirableStorageService) private readonly storageService: ExpirableStorageService) {
    this.expireAfterMs = parseInt(process.env.STORAGE_EXPIRATION ?? '600') * 1000
  }

  @Cron(CronExpression.EVERY_5_MINUTES)
  async deleteOldCredentials() {
    const oldest = new Date(Date.now() - this.expireAfterMs)
    const deleted = await this.storageService.deleteOlderThan(oldest)
    if (deleted) {
      Logger.debug(`Deleted ${deleted} outdated credentials`)
    }
  }
}
