import { StorageService } from '@gaia-x/oidc4vc'

export interface ExpirableStorageService extends StorageService {
  deleteOlderThan(date: Date): Promise<number>
}

export const ExpirableStorageService = 'StorageService'
