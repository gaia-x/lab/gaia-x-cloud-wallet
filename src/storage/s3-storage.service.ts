import { md5 } from 'node-forge'
import {
  S3Client,
  DeleteObjectCommand,
  GetObjectCommand,
  ListObjectsV2Command,
  PutObjectCommand,
  CreateBucketCommand,
  ListBucketsCommand
} from '@aws-sdk/client-s3'
import { StorageService } from '@gaia-x/oidc4vc'

export class S3StorageService implements StorageService {
  private readonly client: S3Client
  private readonly accessKeyId: string
  private readonly secretAccessKey: string
  private readonly endpoint: string
  private readonly region: string
  private readonly bucketName: string
  private bucketReady: Promise<void>

  constructor() {
    if (!process.env.S3_ACCESS_KEY_ID) throw new Error('Missing S3_ACCESS_KEY_ID env variable')
    this.accessKeyId = process.env.S3_ACCESS_KEY_ID
    if (!process.env.S3_SECRET_ACCESS_KEY) throw new Error('Missing S3_SECRET_ACCESS_KEY env variable')
    this.secretAccessKey = process.env.S3_SECRET_ACCESS_KEY
    if (!process.env.S3_ENDPOINT) throw new Error('Missing S3_ENDPOINT env variable')
    this.endpoint = process.env.S3_ENDPOINT
    if (!process.env.S3_REGION) throw new Error('Missing S3_REGION env variable')
    this.region = process.env.S3_REGION
    if (!process.env.S3_BUCKET_NAME) throw new Error('Missing S3_BUCKET_NAME env variable')
    this.bucketName = process.env.S3_BUCKET_NAME

    this.client = new S3Client({
      endpoint: this.endpoint,
      region: this.region,
      credentials: {
        accessKeyId: this.accessKeyId,
        secretAccessKey: this.secretAccessKey
      }
    })
  }

  async initBucket() {
    const buckets = await this.client.send(new ListBucketsCommand())
    if (!buckets.Buckets?.find(bucket => bucket.Name === this.bucketName)) {
      await this.client.send(new CreateBucketCommand({ Bucket: this.bucketName }))
    }
  }

  useBucket() {
    return (this.bucketReady ??= this.initBucket())
  }

  async store(key: string, data: any) {
    await this.useBucket()
    await this.client.send(
      new PutObjectCommand({
        Bucket: this.bucketName,
        Key: this.getStorageKey(key),
        Body: JSON.stringify(data)
      })
    )
  }

  async get(key: string) {
    await this.useBucket()
    const value = (
      await this.client.send(
        new GetObjectCommand({
          Bucket: this.bucketName,
          Key: this.getStorageKey(key)
        })
      )
    ).Body?.toString()
    return value ? JSON.parse(value) : undefined
  }

  async delete(key: string): Promise<void> {
    await this.useBucket()
    await this.client.send(new DeleteObjectCommand({ Bucket: this.bucketName, Key: this.getStorageKey(key) }))
  }

  async deleteOlderThan(date: Date): Promise<number> {
    await this.useBucket()
    let deleted = 0
    const list = await this.client.send(new ListObjectsV2Command({ Bucket: this.bucketName }))
    for (const object of list.Contents ?? []) {
      if (object.LastModified < date) {
        await this.delete(object.Key)
        deleted++
      }
    }
    return deleted
  }

  private getStorageKey(key: string) {
    if (key.length <= 255) {
      return key
    }
    return md5.create().update(key).digest().toHex().substring(0, 255)
  }
}
