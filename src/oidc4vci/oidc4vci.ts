import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import {
  AccessTokenRequest,
  AccessTokenResponse,
  CommonCredentialRequest,
  CreateCredentialOfferURIResult,
  CredentialIssuerMetadataOpts,
  CredentialOfferFormat,
  CredentialOfferPayloadV1_0_11,
  CredentialOfferSession,
  CredentialResponse,
  CredentialSupplierConfig,
  CredentialSupported,
  Grant,
  GrantTypes,
  MetadataDisplay,
  OID4VCICredentialFormat,
  ProofOfPossession
} from '@sphereon/oid4vci-common'
import { AdditionalClaims, ICredential, ICredentialSubject, IIssuer, W3CVerifiableCredential } from '@sphereon/ssi-types'

export class AccessTokenRequestDTO implements AccessTokenRequest {
  @ApiPropertyOptional()
  client_id?: string
  @ApiPropertyOptional()
  code?: string
  @ApiPropertyOptional()
  code_verifier?: string
  @ApiProperty()
  grant_type: GrantTypes
  @ApiProperty()
  'pre-authorized_code': string
  @ApiPropertyOptional()
  redirect_uri?: string
  @ApiPropertyOptional()
  scope?: string
  @ApiPropertyOptional()
  user_pin?: string
}

export class AccessTokenResponseDTO implements AccessTokenResponse {
  @ApiProperty()
  access_token: string
  @ApiPropertyOptional()
  scope?: string
  @ApiPropertyOptional()
  token_type?: string
  @ApiPropertyOptional()
  expires_in?: number
  @ApiPropertyOptional()
  c_nonce?: string
  @ApiPropertyOptional()
  c_nonce_expires_in?: number
  @ApiPropertyOptional()
  authorization_pending?: boolean
  @ApiPropertyOptional()
  interval?: number
}

export class ICredentialDTO implements ICredential {
  @ApiProperty()
  '@context': string | string[]
  @ApiProperty()
  type: string[]
  @ApiProperty()
  issuer: string | IIssuer
  @ApiProperty()
  issuanceDate: string
  @ApiProperty()
  credentialSubject: (ICredentialSubject & AdditionalClaims) | (ICredentialSubject & AdditionalClaims)[]
  @ApiPropertyOptional()
  expirationDate?: string
  @ApiPropertyOptional()
  id?: string
}

export class CommonCredentialRequestDTO implements CommonCredentialRequest {
  @ApiProperty()
  format: OID4VCICredentialFormat
  @ApiPropertyOptional()
  proof?: ProofOfPossession
}

export class CredentialIssuerMetadataOptsDTO implements CredentialIssuerMetadataOpts {
  @ApiPropertyOptional()
  credential_endpoint?: string
  @ApiPropertyOptional()
  batch_credential_endpoint?: string
  @ApiProperty()
  credentials_supported: CredentialSupported[]
  @ApiProperty()
  credential_issuer: string
  @ApiPropertyOptional()
  authorization_server?: string
  @ApiPropertyOptional()
  token_endpoint?: string
  @ApiPropertyOptional()
  display?: MetadataDisplay[]
  @ApiPropertyOptional()
  credential_supplier_config?: CredentialSupplierConfig
}

export class CreateCredentialOfferURIResultDTO implements CreateCredentialOfferURIResult {
  @ApiProperty()
  uri: string
  @ApiPropertyOptional()
  qrCodeDataUri?: string
  @ApiProperty()
  session: CredentialOfferSession
  @ApiPropertyOptional()
  userPin?: string
  @ApiPropertyOptional()
  userPinLength?: number
  @ApiProperty()
  userPinRequired: boolean
}

export class CredentialOfferPayloadDTO implements CredentialOfferPayloadV1_0_11 {
  @ApiProperty()
  credential_issuer: string
  @ApiProperty()
  credentials: (string | CredentialOfferFormat)[]
  @ApiPropertyOptional()
  grants?: Grant
  @ApiPropertyOptional()
  client_id?: string
}

export class CredentialResponseDTO implements CredentialResponse {
  @ApiPropertyOptional()
  credential?: W3CVerifiableCredential
  @ApiProperty()
  format: OID4VCICredentialFormat
  @ApiPropertyOptional()
  transaction_id?: string
  @ApiPropertyOptional()
  acceptance_token?: string
  @ApiPropertyOptional()
  c_nonce?: string
  @ApiPropertyOptional()
  c_nonce_expires_in?: number
}
