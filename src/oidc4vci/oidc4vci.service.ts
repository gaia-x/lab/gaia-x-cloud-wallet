import { CredentialSupportedJwtVcJsonLdAndLdpVc } from '@sphereon/oid4vci-common'
import { Injectable, Inject } from '@nestjs/common'
import { KeyLike } from 'jose'
import { ExpirableStorageService } from '../storage/storage.service'
import { OIDC4VCIService as BaseOIDC4VCIService } from '@gaia-x/oidc4vc'

@Injectable()
export class OIDC4VCIService extends BaseOIDC4VCIService {
  constructor(
    @Inject('josePrivateKey') privateKey: KeyLike,
    @Inject('credentialsSupport') credentialsSupport: CredentialSupportedJwtVcJsonLdAndLdpVc[],
    @Inject(ExpirableStorageService) storageService: ExpirableStorageService
  ) {
    super(privateKey, credentialsSupport, {
      baseUrl: process.env.BASE_URL,
      deleteOnUse: process.env.DELETE_ON_USE === 'true',
      storageService
    })
  }
}
