import { Body, Controller, Get, Header, Headers, HttpCode, Inject, Param, Post } from '@nestjs/common'
import {
  AccessTokenRequest,
  AccessTokenResponse,
  CreateCredentialOfferURIResult,
  CredentialIssuerMetadataOpts,
  CredentialOfferPayloadV1_0_11,
  CredentialResponse,
  CommonCredentialRequest,
  CredentialRequestJwtVcJson
} from '@sphereon/oid4vci-common'
import { KeyLike } from 'jose'
import { PreAuthorizedCodeUtils } from '../util/pre-authorized-code.utils'
import { OIDC4VCIService } from './oidc4vci.service'
import { ICredential } from '@sphereon/ssi-types'
import { ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger'
import {
  AccessTokenRequestDTO,
  AccessTokenResponseDTO,
  CommonCredentialRequestDTO,
  CreateCredentialOfferURIResultDTO,
  CredentialIssuerMetadataOptsDTO,
  CredentialOfferPayloadDTO,
  CredentialResponseDTO,
  ICredentialDTO
} from './oidc4vci'

@ApiTags('oidc4vci')
@Controller()
export class Oidc4VCIController {
  constructor(
    @Inject('josePublicKey') private readonly publicKey: KeyLike,
    private readonly oidc4vciService: OIDC4VCIService
  ) {}

  @ApiOperation({
    summary: 'Retrieve issuer OpenId for Verifiable Credentials issuance issuer metadata'
  })
  @ApiResponse({ type: CredentialIssuerMetadataOptsDTO })
  @Get(['.well-known/openid-credential-issuer', 'auth/.well-known/openid-configuration'])
  @Header('Content-Type', 'application/json')
  getCredentialIssuerMetadata(): CredentialIssuerMetadataOpts {
    return this.oidc4vciService.generateIssuerMetadata()
  }

  @ApiOperation({
    summary: 'Request an access token'
  })
  @ApiResponse({ type: AccessTokenResponseDTO })
  @ApiBody({
    type: AccessTokenRequestDTO,
    description: 'An access token request'
  })
  @Post('token')
  @HttpCode(200)
  @Header('Content-Type', 'application/json')
  requestToken(@Body() accessTokenRequest: AccessTokenRequest): Promise<AccessTokenResponse> {
    return this.oidc4vciService.issueAccessToken(accessTokenRequest)
  }

  @ApiOperation({
    summary: 'Store a credential to be retrievable later through a credential request'
  })
  @ApiResponse({ type: CreateCredentialOfferURIResultDTO })
  @ApiBody({
    type: ICredentialDTO,
    description: 'A credential to create offer from'
  })
  @Post('requestCredential')
  @HttpCode(200)
  @Header('Content-Type', 'application/json')
  async requestCredential(@Body() credential: ICredential): Promise<CreateCredentialOfferURIResult> {
    const preAuthorizedCode = PreAuthorizedCodeUtils.generate()
    return await this.oidc4vciService.createCredentialOffer(preAuthorizedCode, credential)
  }

  @ApiOperation({
    summary: 'Request a credential offer from an authorization code'
  })
  @ApiResponse({ type: CredentialOfferPayloadDTO })
  @Get('credentialOffer/:preAuthorizationCode')
  @HttpCode(200)
  @Header('Content-Type', 'application/json')
  async getCredentialOffer(@Param('preAuthorizationCode') preAuthorizedCode: string): Promise<CredentialOfferPayloadV1_0_11> {
    const credentialOfferSession = await this.oidc4vciService.getCredentialOffer(preAuthorizedCode)
    if (!credentialOfferSession) {
      throw new Error('Session expired, please renew it')
    }
    return credentialOfferSession.credentialOffer.credential_offer
  }

  @ApiOperation({
    summary: 'Present a credential from the given request'
  })
  @ApiResponse({ type: CredentialResponseDTO })
  @ApiBody({
    type: CommonCredentialRequestDTO,
    description: 'A credential request'
  })
  @Post('credential')
  @HttpCode(200)
  @Header('Content-Type', 'application/json')
  async issueCredential(
    @Headers('Authorization') authz: string,
    @Body()
    credentialRequest: CommonCredentialRequest & CredentialRequestJwtVcJson
  ): Promise<CredentialResponse> {
    const preAuthorizedCode: string = await PreAuthorizedCodeUtils.extractFromJwt(authz, this.publicKey)
    return this.oidc4vciService.issueCredential(preAuthorizedCode, credentialRequest)
  }
}
