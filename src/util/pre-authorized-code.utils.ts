import * as jose from 'jose'
import { KeyLike } from 'jose'
import { v4 as uuid } from 'uuid'

export class PreAuthorizedCodeUtils {
  private static readonly BEARER_REGEX: RegExp = /Bearer (.+)/i

  static generate(): string {
    return uuid()
  }

  static async extractFromJwt(authorizationHeader: string, publicKey: KeyLike): Promise<string> {
    const jwt: string = authorizationHeader.match(PreAuthorizedCodeUtils.BEARER_REGEX)![1]
    const { payload } = await jose.jwtVerify(jwt, publicKey)
    return payload.preAuthorizedCode! as string
  }
}
