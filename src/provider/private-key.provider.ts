import { FactoryProvider } from '@nestjs/common'
import * as jose from 'jose'
import { KeyLike } from 'jose'
import * as process from 'process'

const factoryProvider: FactoryProvider<KeyLike> = {
  provide: 'josePrivateKey',
  async useFactory(): Promise<KeyLike> {
    return await jose.importPKCS8(process.env.PRIVATE_KEY, null)
  }
}

export default factoryProvider
