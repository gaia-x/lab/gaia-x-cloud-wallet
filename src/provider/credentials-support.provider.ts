import { CredentialSupported } from '@sphereon/oid4vci-common'

const membership: CredentialSupported[] = [
  { type: 'vcard:Organization', name: 'Vcard Gaia-X Membership', backgroundColor: '#46daff', textColor: '#000094' },
  {
    type: 'org:ProgramMembership',
    name: 'Schema.Org Gaia-X Membership',
    backgroundColor: '#cecece',
    textColor: '#1a1a1a'
  }
].map(o => ({
  format: 'jwt_vc_json',
  id: o.type,
  types: ['VerifiableCredential', o.type],
  display: [
    {
      name: o.name,
      locale: 'en-US',
      logo: {
        url: 'https://www.systnaps.com/wp-content/uploads/2021/04/Gaia-X_Logo_Standard_RGB_Transparent_210401-e1617719976112.png',
        alt_text: 'The Gaia-X logo'
      },
      background_color: o.backgroundColor,
      text_color: o.textColor
    }
  ]
}))

export default membership
