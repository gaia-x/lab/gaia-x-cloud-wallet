import { FactoryProvider } from '@nestjs/common'
import * as jose from 'jose'
import { KeyLike } from 'jose'
import * as process from 'process'

const factoryProvider: FactoryProvider<KeyLike> = {
  provide: 'josePublicKey',
  async useFactory(): Promise<KeyLike> {
    return await jose.importSPKI(process.env.PUBLIC_KEY, 'ES256')
  }
}

export default factoryProvider
