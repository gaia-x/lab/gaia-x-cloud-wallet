# [1.1.0](https://gitlab.com/gaia-x/lab/gaia-x-cloud-wallet/compare/v1.0.1...v1.1.0) (2024-07-30)


### Bug Fixes

* reduce credential types to improve the UX ([4177716](https://gitlab.com/gaia-x/lab/gaia-x-cloud-wallet/commit/41777167cc1d4054a0b3a0d17d099526e8cd606f))
* use app path in swagger ui setup url ([019a04a](https://gitlab.com/gaia-x/lab/gaia-x-cloud-wallet/commit/019a04a43c016d9f0eab941b6a241ee211c8b4cc))


### Features

* **LAB-581:** improve credentials lifecycle ([003fd38](https://gitlab.com/gaia-x/lab/gaia-x-cloud-wallet/commit/003fd38d2816fcf7693cde30ab707205f1a793df))
* use gaia-x/oidc4vc package ([81c0f4f](https://gitlab.com/gaia-x/lab/gaia-x-cloud-wallet/commit/81c0f4f005590cf590dbcdac6b0bc90d69c13a7b))

## [1.0.1](https://gitlab.com/gaia-x/lab/gaia-x-cloud-wallet/compare/v1.0.0...v1.0.1) (2024-05-21)


### Bug Fixes

* update failing e2e test ([b77322a](https://gitlab.com/gaia-x/lab/gaia-x-cloud-wallet/commit/b77322a6a69bf3feb224d24ca8991b07dcc46840))

# 1.0.0 (2024-05-21)


### Bug Fixes

* update credential offer format to jwt_vc_json ([543dbef](https://gitlab.com/gaia-x/lab/gaia-x-cloud-wallet/commit/543dbeffbceeb7505c83088c1dd0ae91bf308fa2))


### Features

* add compliance credential type ([2032f55](https://gitlab.com/gaia-x/lab/gaia-x-cloud-wallet/commit/2032f55f32bfc1f9b7e24867de50db65e48220fc))
* add membership credential description ([39ee756](https://gitlab.com/gaia-x/lab/gaia-x-cloud-wallet/commit/39ee756dba7f120b728d7bc3883b2542ce0c3ebb))
* create base implementation of oidc4vci ([9f7f14a](https://gitlab.com/gaia-x/lab/gaia-x-cloud-wallet/commit/9f7f14ae8a601c3e6f06b7da5e123296b33b728c))
* **membership-credential:** add several membership credential types ([423a723](https://gitlab.com/gaia-x/lab/gaia-x-cloud-wallet/commit/423a723808e7793c4c62ce5c4712fcd279d133b9))
