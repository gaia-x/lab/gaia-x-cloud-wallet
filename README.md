# Gaia-X Cloud Wallet

The Gaia-X Cloud Wallet project aims to securely store and present [Verifiable Credential](https://www.w3.org/TR/vc-data-model-2.0/)
using the OpenID protocol for Verifiable Credential issuance.

This project provides the necessary endpoints for:
- Pushing a credential to the wallet using the `HTTP POST /requestCredential`.
- Serving the credential according to the OpenID for Verifiable Credential Issuance draft 13.

## Running locally

Please use a Node.js LTS version and install the required dependencies with the following commands.

```bash
nvm install
npm install

```bash
cp .env.example .env
```

```bash
npm run start:dev
```


## Deployment

A helm chart is provided inside <a href="k8s/gx-cloud-wallet">k8s/gx-cloud-wallet</a> folder.

It provides several environment variables for the application:

| Env Variable        | Name in values file            | Default value                                                    | Note                                                    |
|---------------------|--------------------------------|------------------------------------------------------------------|---------------------------------------------------------|
| APP_PATH            | ingress.hosts[0].paths[0].path | /main                                                            | Deployment path of the application                      |
| BASE_URL            | BASE_URL                       | https://<ingress.hosts[0].host>/<ingress.hosts[0].paths[0].path> | URL of the deployed application                         |
| PRIVATE_KEY         | PRIVATE_KEY                    | base64 value of "empty"                                          | The private key                                         |
| PUBLIC_KEY          | PUBLIC_KEY                     | base64 value of "empty"                                          | The public key                                          |
| DELETE_ON_USE       | DELETE_ON_USE                  | false                                                            | true to delete credentials upon usage                   |
| STORAGE_EXPIRATION  | STORAGE_EXPIRATION             | 600                                                              | Time in seconds before credential deletion from storage |

By default, credentials to present are kept in memory. In case service is restarted or load-balanced, credentials might become unavailable.
Permanent storage using S3 bucket is available to avoid this:

| Env Variable         | Name in values file            | Default value | Note              |
|----------------------|--------------------------------|---------------|-------------------|
| STORAGE              | STORAGE                        | in-memory     | Set value to `S3` |
| S3_ACCESS_KEY_ID     | S3_ACCESS_KEY_ID               |               | S3 access key     |
| S3_SECRET_ACCESS_KEY | S3_SECRET_ACCESS_KEY           |               | S3 secret         |
| S3_ENDPOINT          | S3_ENDPOINT                    |               | S3 endpoint       |
| S3_REGION            | S3_REGION                      |               | S3 region         |
| S3_BUCKET_NAME       | S3_BUCKET_NAME                 |               | S3 bucket name    |


## Note

### PRIVATE_KEY and PUBLIC_KEY format

PRIVATE_KEY and PUBLIC_KEY must be base64 of PEM key format without linecode. Sample command to obtain text is here:


	$ tr -d '\n'  < public_or_private_key.pem  |base64 -w0
	LS0tLS1CRUdJTiB....
